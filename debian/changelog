gimp-help (2.10.34-2) unstable; urgency=medium

  * Upload to unstable, targetting transition to bookworm.

 -- Jordi Mallach <jordi@debian.org>  Wed, 03 May 2023 11:00:29 +0200

gimp-help (2.10.34-1) experimental; urgency=medium

  * New upstream release
  * Add new binary packages for cs, da, en_GB, fa, fi, hr, hu, lt, pt,
    ro, uk and zh_CN.
  * Drop obsolete override for dh_clean.
  * Drop all patches.
  * Set gbp's upstream-vcs-tag to GIMP_HELP_%(version%.%_)s.

 -- Jordi Mallach <jordi@debian.org>  Mon, 01 May 2023 01:53:14 +0200

gimp-help (2.10.0-1) unstable; urgency=medium

  * Team upload.
  * Add Vcs-* fields
  * debian/gbp.conf: Disable upstream-vcs-tag for now
  * New upstream release
  * Drop obsolete autogen patches
  * Add python3 patch from Fedora
  * Switch python build-deps to python3 equivalent (Closes: #951823, #967139)
  * Pass NOCONFIGURE=1 to autoreconf/autogen.sh
  * Add debian/patches/missing-languages.patch
  * debian/rules: Disable parallel builds
  * Add patch to fix up sl translation syntax error
  * Add patch fixing breakage in building swedish translation

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 03 Sep 2020 19:55:13 +0200

gimp-help (2.8.2-2) unstable; urgency=medium

  * Team upload.

  [ Andreas Henriksson ]
  * Replace gnome-doc-utils with python and python-libxml2 build-deps
    - gimp-help uses bundled tools/xml2po rather than gnome-doc-utils.
    (Closes: #947526)

  [ Debian Janitor ]
  * Use correct machine-readable copyright file URI.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set field Upstream-Name in debian/copyright.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 10 Feb 2020 13:27:31 +0100

gimp-help (2.8.2-1) unstable; urgency=medium

  * Add patch from previous upload to debian/patches/series
  * Switch maintainer to Debian GNOME team to match gimp
  * Bump debhelper compat to 11
  * Use dh_missing --list-missing
  * debian/copyright: Add GFDL short license text
  * Bump Standards-Version to 4.2.1

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 05 Oct 2018 00:42:13 -0400

gimp-help (2.8.2-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Add upstream fix for FTBFS with automake 1.16,
    thanks to Juhani Numminen. (Closes: #906358)

 -- Adrian Bunk <bunk@debian.org>  Wed, 19 Sep 2018 21:21:52 +0300

gimp-help (2.8.2-0.1) unstable; urgency=medium

  * Non-maintainer upload.

 -- Matthias Klose <doko@debian.org>  Mon, 31 Aug 2015 22:26:19 +0200

gimp-help (2.8.2-0ubuntu2) wily; urgency=medium

  * debian/gimp-help-ja.doc-base: Typo corrected (LP: #1434133).

 -- Gunnar Hjalmarsson <gunnarhj@ubuntu.com>  Thu, 14 May 2015 04:12:00 +0200

gimp-help (2.8.2-0ubuntu1) vivid; urgency=medium

  * New upstream version. Closes: #737102.
  * Fix build failure. Closes: #747958, #724912.
  * New packages for Catalan, Greek, Portuguese, Slovenian.

 -- Matthias Klose <doko@ubuntu.com>  Fri, 13 Mar 2015 22:42:20 +0100

gimp-help (2.6.1-1) unstable; urgency=low

  * New upstream release (Closes: #523295, #550535)
    - git archive GIMP_HELP_2_6_1 | gzip > ../gimp-help_2.6.1.orig.tar.gz
  * Changed location of upstream source in copyright file
  * Use debhelper 7 with short rules file
  * Use machine-readable copyright file

 -- Ari Pollak <ari@debian.org>  Sat, 14 Aug 2010 01:02:20 -0400

gimp-help (2.4.1-2) unstable; urgency=low

  * Don't use a custom version of dh_installdocs anymore (Closes: #521933)

 -- Ari Pollak <ari@debian.org>  Sun, 12 Apr 2009 16:36:19 -0400

gimp-help (2.4.1-1) unstable; urgency=low

  * New upstream release
  * Remove executable bit from images (Closes: #394907)

 -- Ari Pollak <ari@debian.org>  Fri, 11 Apr 2008 19:05:53 -0400

gimp-help (2.4.0-2) unstable; urgency=low

  * Changed gimp-helpbrowser dependency to gimp
  * Remove bashism in debian/rules when cleaning up images from missing
    languages (Closes: #459191)

 -- Ari Pollak <ari@debian.org>  Fri, 04 Jan 2008 15:14:19 -0500

gimp-help (2.4.0-1) unstable; urgency=low

  * New upstream release

 -- Ari Pollak <ari@debian.org>  Sat, 01 Dec 2007 14:06:09 -0500

gimp-help (2+0.13-1) unstable; urgency=low

  * New upstream release
  * Fix description for gimp-help-no (Closes: #421615)

 -- Ari Pollak <ari@debian.org>  Fri, 03 Aug 2007 12:21:37 -0400

gimp-help (2+0.12-1) unstable; urgency=low

  * New upstream release
     - Add Korean, Norwegian, and Russian translations
  * Remove old compatibility conflicts & replaces that were from pre-sarge

 -- Ari Pollak <ari@debian.org>  Sun, 11 Mar 2007 08:56:21 -0400

gimp-help (2+0.10-2) unstable; urgency=low

  * Add Build-Depend on docbook-xml for 4.3 DTDs, and apply patch
    from Mike Hommey to allow building with libxslt 1.1.8 (Closes: #397623)
  * Make clean rule better

 -- Ari Pollak <ari@debian.org>  Fri, 10 Nov 2006 23:41:27 -0500

gimp-help (2+0.10-1) unstable; urgency=low

  * New upstream release
    - Ignore the Croatian translation since there's barely anything
      translated

 -- Ari Pollak <ari@debian.org>  Wed, 12 Apr 2006 19:03:18 -0400

gimp-help (2+0.9-1) unstable; urgency=low

  * New upstream release (Closes: #327806)

 -- Ari Pollak <ari@debian.org>  Mon, 12 Sep 2005 13:59:21 -0400

gimp-help (2+0.8-3) unstable; urgency=low

  * Add imagemagick to build-depends (Closes: #315036)

 -- Ari Pollak <ari@debian.org>  Mon, 20 Jun 2005 11:54:24 -0400

gimp-help (2+0.8-2) unstable; urgency=low

  * Correct lang=en to lang=it in src/concepts/selection.xml (Closes: #314885)
  * Manually add /usr/share/doc/gimp-help-<lang> symlinks in postinst for
    all languages except it and nl if those exist as empty directories
  * Include upstream NEWS file in docs (Closes: #302856)

 -- Ari Pollak <ari@debian.org>  Sun, 19 Jun 2005 13:04:53 -0400

gimp-help (2+0.8-1) unstable; urgency=low

  * New upstream release

 -- Ari Pollak <ari@debian.org>  Tue, 17 May 2005 19:11:04 -0400

gimp-help (2+0.7-5) unstable; urgency=high

  * Fix doc-base entry for gimp-help-zh-cn to point to correct directory
    name (Closes: #308228)

 -- Ari Pollak <ari@debian.org>  Sun,  8 May 2005 18:04:08 -0400

gimp-help (2+0.7-4) unstable; urgency=low

  * Correct lang=en to lang=cs in ch. 2 sec. 6.3. This is already fixed
    in upstream CVS, from GNOME bug 170140. (Closes: #170140)

 -- Ari Pollak <ari@debian.org>  Mon,  2 May 2005 16:06:22 -0400

gimp-help (2+0.7-3) unstable; urgency=low

  * Oops, add a section to the *.doc-base files, and actually install them
  * Fix the order of the directories in the *.links files

 -- Ari Pollak <ari@debian.org>  Wed,  9 Mar 2005 09:18:59 -0500

gimp-help (2+0.7-2) unstable; urgency=low

  * Actually put the appropriate files into gimp-help-cs
  * Make /usr/share/doc/gimp-help-<lang> symlink to gimp-help-common,
    and link /usr/share/gimp/2.0/help to /usr/share/doc/gimp-help-common/html.
    (Closes: #298428)
  * Create debian/gimp-help-<lang>.doc-base files

 -- Ari Pollak <ari@debian.org>  Mon,  7 Mar 2005 10:16:22 -0500

gimp-help (2+0.7-1) unstable; urgency=low

  * New upstream release
  * Add a -cs package for Czech

 -- Ari Pollak <ari@debian.org>  Fri, 18 Feb 2005 20:27:34 -0500

gimp-help (2+0.6-3) unstable; urgency=low

  * Rebuild, depending on xsltproc >= 1.1.12, since it has now hit unstable.
    This should fix the problem where the back cover indexes are not generated.
    (Closes: #286831)

 -- Ari Pollak <ari@debian.org>  Thu, 17 Feb 2005 01:41:14 -0500

gimp-help (2+0.6-2) unstable; urgency=low

  * Rebuild for unstable

 -- Ari Pollak <ari@debian.org>  Mon, 20 Dec 2004 18:43:22 -0500

gimp-help (2+0.6-1) experimental; urgency=low

  * New upstream release for experimental
    - if GIMP 2.2 will be uploaded to unstable, so will this.
  * Fix enormous .diff file
  * Don't copy config.{sub,guess}, they're not used here
  * Language packs now have versioned depends on gimp-help-common
  * Remove html/, pdf/, and xml/ on clean

 -- Ari Pollak <ari@debian.org>  Sun, 19 Dec 2004 15:20:04 -0500

gimp-help (2+0.5-1) experimental; urgency=low

  * New upstream release for experimental, since it contains
    documentation updated for GIMP 2.2 (already in experimental).
  * Add gimp-help-zh-cn package for Chinese Simplified
  * Add --enable-build to configure options to rebuild output files,
    since otherwise the French translation won't be included.
    Add xsltproc and docbook-xsl to build-depends appropriately.

 -- Ari Pollak <ari@debian.org>  Mon,  8 Nov 2004 10:07:41 -0500

gimp-help (2+0.4-1) unstable; urgency=low

  * New upstream release

 -- Ari Pollak <ari@debian.org>  Fri, 13 Aug 2004 09:07:02 -0400

gimp-help (2+0.3-5) unstable; urgency=low

  * Remove gimp-help dummy package (Closes: #263855)
  * Depend on gimp-helpbrowser | www-browser, not gimp

 -- Ari Pollak <ari@debian.org>  Thu,  5 Aug 2004 21:48:48 -0400

gimp-help (2+0.3-4) unstable; urgency=low

  * add gimp-helpbrowser option to depends, remove gimp

 -- Ari Pollak <ari@debian.org>  Fri,  9 Jul 2004 10:40:14 -0400

gimp-help (2+0.3-3) unstable; urgency=low

  * Replaces & Conflicts: gimp-help (<< 2+0.3-1) (Closes: #255281)

 -- Ari Pollak <ari@debian.org>  Sat, 19 Jun 2004 21:04:03 -0400

gimp-help (2+0.3-2) unstable; urgency=low

  * Make all language packages Recommend gimp-help-en, and explain why

 -- Ari Pollak <ari@debian.org>  Sat, 19 Jun 2004 13:04:33 -0400

gimp-help (2+0.3-1) unstable; urgency=low

  * New upstream release
  * Split gimp-help into language-specific packages, make gimp-help a
    dummy and depend on gimp-help-en for now

 -- Ari Pollak <ari@debian.org>  Sun,  6 Jun 2004 08:52:50 -0400

gimp-help (2+0.2-2) unstable; urgency=low

  * Suggest gimp, not gimp1.3 (Closes: #247390)

 -- Ari Pollak <ari@debian.org>  Tue,  4 May 2004 18:15:52 -0400

gimp-help (2+0.2-1) unstable; urgency=low

  * New upstream release

 -- Ari Pollak <ari@debian.org>  Wed, 24 Mar 2004 21:54:41 -0500

gimp-help (2+0.1-1) unstable; urgency=low

  * Initial Release. (Closes: #236987)

 -- Ari Pollak <ari@debian.org>  Tue,  9 Mar 2004 00:26:21 -0500
